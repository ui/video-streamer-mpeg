import glob
import pytest
import os

import numpy
from PIL import Image, ImageChops

from video_streamer_mpeg.streamer import main

DIR_PATH = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture(scope="session")
def result_dir(tmpdir_factory):
    p = tmpdir_factory.mktemp("data")
    yield str(p)


def execute_sim(params):
    try:
        main(params)
    except SystemExit as e:
        assert e.args[0] == 0, "The streamer simulator terminated with an error"
        return
    raise RuntimeError("The streamer simulator is supposed to return with SysExit")


def compare(result_path, reference_path):
    with Image.open(reference_path) as reference:
        with Image.open(result_path) as result:
            img_diff = ImageChops.difference(reference, result)
            diff = numpy.array(img_diff)

            assert diff.sum() == pytest.approx(0)


def test_frame_types(result_dir):
    frames = glob.glob(f"{DIR_PATH}/frames/*.raw")
    for f in frames:
        basename = os.path.basename(f).replace(".raw", "")
        if "_2_" in basename:
            # Y32 is not supported by default
            continue
        result_path = f"{result_dir}/{basename}.jpg"
        reference_path = f"{DIR_PATH}/results/{basename}.jpg"
        execute_sim(["-t", "sim", "-d", f, "-j", result_path])
        compare(result_path, reference_path)


def test_rotation(result_dir):
    frames = glob.glob(f"{DIR_PATH}/frames/*.raw")
    for f in frames:
        basename = os.path.basename(f).replace(".raw", "")
        if "_2_" in basename:
            # Y32 is not supported by default
            continue
        for rot in [90, 270]:
            result_path = f"{result_dir}/{basename}_rot{rot}.jpg"
            reference_path = f"{DIR_PATH}/results/{basename}_rot{rot}.jpg"
            execute_sim(["-t", "sim", "-d", f, "-j", result_path, "-r", str(rot)])
            compare(result_path, reference_path)


def test_bayer_12bpp(result_dir):
    basename = "basler_aca1920-40gc_11_1936x1216_frame0"
    frame = f"{DIR_PATH}/frames/{basename}.raw"
    result_path = f"{result_dir}/{basename}_12bpp.jpg"
    reference_path = f"{DIR_PATH}/results/{basename}_12bpp.jpg"
    execute_sim(["-t", "sim", "-d", frame, "-j", result_path, "-bpp", "12"])
    compare(result_path, reference_path)


def test_bayer_halftone(result_dir):
    basename = "basler_aca1920-40gc_11_1936x1216_frame0"
    frame = f"{DIR_PATH}/frames/{basename}.raw"
    result_path = f"{result_dir}/{basename}_ht.jpg"
    reference_path = f"{DIR_PATH}/results/{basename}_ht.jpg"
    execute_sim(["-t", "sim", "-d", frame, "-j", result_path, "-bpp", "12", "-ht"])
    compare(result_path, reference_path)


def test_y32_normalized(result_dir):
    basename = "simulator_prefetched_2_16x16_frame0"
    frame = f"{DIR_PATH}/frames/{basename}.raw"
    result_path = f"{result_dir}/{basename}_norm.png"
    reference_path = f"{DIR_PATH}/results/{basename}_norm.png"
    execute_sim(["-t", "sim", "-d", frame, "--normalize", "-j", result_path])
    compare(result_path, reference_path)
