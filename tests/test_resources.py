import os.path
from video_streamer_mpeg.resources import get_resource


def test_resource():
    """Check that the package can load the rexpected resources"""
    assert os.path.exists(get_resource("cursor.json"))
