A websocket based mpeg1 video streamer.

Uses FFMPEG to convert frames to mpeg and bayer decode as needed.

# Install from source

To install
```
conda create -n video-streamer
conda activate video-streamer
const install --file requirements-conda.txt
pip install -e .
```

# Install without source

To install
```
conda create -n video-streamer
conda activate video-streamer
conda install --yes --file https://gitlab.esrf.fr/ui/video-streamer-mpeg/-/raw/master/requirements-conda.txt
pip install git+https://gitlab.esrf.fr/ui/video-streamer-mpeg
```

# Start the server

To start the streamer
```
video-streamer -d id00/limaccds/simulator
```

With ssl:
```
video-streamer -d id00/limaccds/simulator -cert <path/to/cert> -key <path/to/key>
```
