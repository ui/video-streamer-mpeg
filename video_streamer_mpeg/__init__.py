# -*- coding: utf-8 -*-
"""video-streamer-mpeg main package
"""
from . import release

__version__ = release.version
version_info = release.version_tuple
