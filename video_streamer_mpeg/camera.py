import logging
from abc import ABC, abstractmethod
import numpy as np

logger = logging.getLogger(__name__)


def lut_display(image, display_min, display_max):
    """Lookup table for halftoning

    This is 5x faster than doing the maths directly
    https://stackoverflow.com/questions/14464449/using-numpy-to-efficiently-convert-16-bit-image-data-to-8-bit-for-display-with
    """
    lut = np.arange(2**16, dtype="uint16")
    lut = display(lut, display_min, display_max)
    return np.take(lut, image)


def display(image, display_min, display_max):
    """Halftone an frame from 16bit -> 8bit per pixel"""
    image.clip(display_min, display_max, out=image)
    image -= display_min
    np.floor_divide(
        image, (display_max - display_min + 1) / 256, out=image, casting="unsafe"
    )
    return image.astype(np.uint8)


class CameraBase(ABC):
    """Base camera frame grabber"""

    # ffmpeg pixel format mapping
    # https://ffmpeg.org/doxygen/trunk/pixfmt_8h.html
    _formats = {
        "YUV422PACKED": "uyvy422",
        "BAYER_RG16": "bayer_rggb16",
        "BAYER_BG16": "bayer_bggr16",
        "BAYER_RG8": "bayer_rggb8",
        "BAYER_BG8": "bayer_bggr8",
        "RGB24": "rgb24",
        "BGR24": "bgr24",
        "Y8": "gray8",
        "Y16": "gray16",
    }

    _formats_normalize = {"Y8": "gray8", "Y16": "gray8", "Y32": "gray8"}

    _formats_half_tone = {"BAYER_RG16": "bayer_rggb8", "BAYER_BG16": "bayer_bggr8"}

    _half_tone = False
    _bpp = None
    _normalize = False

    def set_options(self, half_tone: bool, bpp: int, normalize: bool):
        """Set bayer options
        * Enable/disable halftoning
        * Set bpp for scaling
        """
        self._half_tone = half_tone
        self._args_bpp = bpp
        self._normalize = normalize

    def bpp(self) -> int:
        """Return the bits per pixel provided by this camera"""
        return None

    @abstractmethod
    def raw_frame(self, save=False) -> np.ndarray:
        """Return a numpy array from a camera"""
        return None

    def frame(self) -> np.ndarray:
        """Return a frame with 8bit per pixel

        Returns:
            raw (ndarray): Frame data
        """
        raw = self.raw_frame()

        # If bpp is specified rescale the data accordingly
        bpp = self.bpp()
        if self._args_bpp:
            bpp = self._args_bpp

        if bpp is not None and "16" in self.pix_format().lower():
            raw = raw.view(np.uint16) << (16 - bpp)

        if self._half_tone and self.pix_format() in self._formats_half_tone:
            raw = lut_display(raw.view(np.uint16), 0, 1 << 16)

        if self._normalize:
            vmin, vmax = raw.min(), raw.max()
            raw = (255 * (raw.astype(float) - vmin) / (vmin - vmax)).astype(np.uint8)

        return raw

    def format(self) -> str:
        """Return an FFMPEG compatible pixel format from the camera format

        Returns
            format (str): FFMPEG pix_fmt
        """
        pix_fmt = self.pix_format()
        try:
            if self._half_tone:
                return self._formats_half_tone[pix_fmt]
            elif self._normalize:
                return self._formats_normalize[pix_fmt]
            else:
                return self._formats[pix_fmt]
        except KeyError:
            if pix_fmt in self._formats_normalize:
                raise KeyError(
                    f"Such pixel format {pix_fmt} is only supported with --normalize."
                )
            raise KeyError(f"No such pixel format {pix_fmt}")

    @abstractmethod
    def size(self) -> tuple:
        """Return the image width and height

        Returns:
            size (tuple): width, height
        """
        pass

    @abstractmethod
    def pix_format(self) -> str:
        """Returns the video pixel format

        Returns:
            format(string): BAYER_RG16, RGB24, etc"""
        pass

    def manual_rotation(self) -> int:
        """If the image needs manual rotation based on a defined rotation"""
        return 0
