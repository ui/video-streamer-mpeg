import os
import logging
import requests
from requests.auth import HTTPDigestAuth
from io import BytesIO
from PIL import Image

import numpy

from .camera import CameraBase

logger = logging.getLogger(__name__)


class JPEGCamera(CameraBase):
    """Generate a video stream from a single jpeg frame

    This can be used with webcams that provide the most recent image as a jpeg
    (not mjpeg). This implementation will feed these into ffmpeg and convert
    to an mpeg stream.

    TODO: The conversion could probably be optimised if higher framerates are
    needed. At the moment the frame conversion is:
         jpeg -> numpy -> ffmpeg
    quicker would probably be:
        jpeg -> ffmpeg
    but requires reworking of the ffmpeg task to take `-f image2` rather than
    `-f raw`
    """

    def __init__(self, url, username=None, password=None):
        self.image_width = None
        self.image_height = None
        self.video_mode = None

        self._url = url

        self._auth = None
        if username and password:
            self._auth = HTTPDigestAuth(username, password)

    def _get_frame(self):
        response = requests.get(self._url, auth=self._auth)
        if response.status_code == 200:
            image = Image.open(BytesIO(response.content))
            if image.mode != "RGB":
                image = image.convert("RGB")
            return image
        else:
            logger.warning(f"{self._url} returned {response.status_code}")

    def size(self):
        image = self._get_frame()
        if image and self.image_height is None and self.image_width is None:
            self.image_width, self.image_height = image.size

        return (self.image_width, self.image_height)

    def pix_format(self):
        return "RGB24"

    def manual_rotation(self):
        return None

    def raw_frame(self, save=False):
        image = self._get_frame()
        if image:
            raw_data = numpy.array(image)

            if save:
                file_template = (
                    f"jpeg_{self.image_width}x{self.image_height}_frame%s.raw"
                )
                no = 0
                filename = file_template % no
                while os.path.exists(filename):
                    no += 1
                    filename = file_template % no

                print(f"Saving: {filename}")
                with open(filename, "wb") as f:
                    f.write(raw_data)

            return raw_data
        return numpy.array([])
