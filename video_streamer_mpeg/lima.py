import os
import struct
import time
import logging
import enum
import numpy

try:
    import tango
except ImportError:
    tango = None

from .camera import CameraBase

logger = logging.getLogger(__name__)

VIDEO_HEADER_FORMAT = "!IHHqiiHHHH"
VIDEO_MAGIC = struct.unpack(">I", b"VDEO")[0]
HEADER_SIZE = struct.calcsize(VIDEO_HEADER_FORMAT)


class VIDEO_MODES(enum.IntEnum):
    # From https://github.com/esrf-bliss/Lima/blob/master/common/include/lima/Constants.h#L118
    Y8 = 0
    Y16 = 1
    Y32 = 2
    Y64 = 3
    RGB555 = 4
    RGB565 = 5
    RGB24 = 6
    RGB32 = 7
    BGR24 = 8
    BGR32 = 9
    BAYER_RG8 = 10
    BAYER_RG16 = 11
    BAYER_BG8 = 12
    BAYER_BG16 = 13
    I420 = 14
    YUV411 = 15
    YUV422 = 16
    YUV444 = 17
    YUV411PACKED = 18
    YUV422PACKED = 19
    YUV444PACKED = 20


# Mapping used for direct conversion from raw data to numpy array
MODE_TO_NUMPY = {
    VIDEO_MODES.Y8: numpy.uint8,
    VIDEO_MODES.Y16: numpy.uint16,
    VIDEO_MODES.Y32: numpy.int32,
    VIDEO_MODES.Y64: numpy.int64,
}


def parse_frame(raw_data):
    (
        magic,
        header_version,
        image_mode,
        image_frameNumber,
        image_width,
        image_height,
        endian,
        header_size,
        pad0,
        pad1,
    ) = struct.unpack(VIDEO_HEADER_FORMAT, raw_data[:HEADER_SIZE])

    if magic != VIDEO_MAGIC or header_version != 1:
        raise IndexError("Bad image header.")
    if image_frameNumber < 0:
        raise IndexError("Image (from Lima live interface) not available yet.")

    dtype = MODE_TO_NUMPY.get(image_mode, numpy.uint8)
    data = numpy.frombuffer(raw_data[HEADER_SIZE:], dtype=dtype)

    return (data, {"width": image_width, "height": image_height, "mode": image_mode})


class LimaCamera(CameraBase):
    def __init__(self, device):
        self.image_width = None
        self.image_height = None
        self.video_mode = None

        self.lima = tango.DeviceProxy(device)

    def size(self):
        self.raw_frame()

        # If size is none capture a frame to get proper image size
        if self.image_width is None:
            self.lima.prepareAcq()
            self.lima.startAcq()

            while self.lima.last_image_ready == -1:
                time.sleep(0.5)

            self.raw_frame()

        return (self.image_width, self.image_height)

    def bpp(self):
        # TODO: This returns 8bpp for YUV even though YUV is 16bpp?
        return int(self.lima.image_type.replace("Bpp", ""))

    def pix_format(self):
        if self.video_mode is None:
            self.video_mode = self.lima.video_mode

        return self.video_mode

    def manual_rotation(self):
        try:
            rotation = int(self.lima.image_rotation)
        # Lima returns 'NONE' in the case of no roation
        except ValueError:
            rotation = 0
        return rotation

    def raw_frame(self, save=False):
        # Thieved from Bliss https://gitlab.esrf.fr/bliss/bliss/blob/master/bliss/data/lima.py#L79
        _, raw_data = self.lima.video_last_image

        if len(raw_data) > HEADER_SIZE:
            data, info = parse_frame(raw_data)

            if save:
                camera = self.lima.camera_type
                model = self.lima.camera_model
                file_template = f"{camera.lower()}_{model.lower()}_{info['mode']}_{info['width']}x{info['height']}_frame%s.raw"
                no = 0
                filename = file_template % no
                while os.path.exists(filename):
                    no += 1
                    filename = file_template % no

                print(f"Saving: {filename}")
                with open(filename, "wb") as f:
                    f.write(raw_data)

            if self.image_width is None or self.image_height is None:
                self.image_width = info["width"]
                self.image_height = info["height"]

            return data

        return numpy.array([])
