import logging

from .camera import CameraBase
from .lima import parse_frame

logger = logging.getLogger(__name__)

video_modes = {
    0: "Y8",
    1: "Y16",
    2: "Y32",
    3: "Y64",
    6: "RGB24",
    7: "RGB32",
    8: "BGR24",
    9: "BGR32",
    10: "BAYER_RG8",
    11: "BAYER_RG16",
    12: "BAYER_BG8",
    13: "BAYER_BG16",
    14: "I420",
    19: "YUV422PACKED",
}


class SimulatorCamera(CameraBase):
    def __init__(self, device):
        self.image_frame = device
        self.rotation = 0

        self.image_width = None
        self.image_height = None
        self.video_mode = None

    def set_rotation(self, rotation):
        self.rotation = rotation

    def size(self):
        if self.image_width is None or self.image_height is None:
            self.raw_frame()

        return (self.image_width, self.image_height)

    def pix_format(self):
        if self.video_mode is None:
            self.raw_frame()

        return self.video_mode

    def manual_rotation(self):
        return self.rotation

    def raw_frame(self, save=False):
        with open(self.image_frame, "rb") as f:
            raw_data = f.read()
            data, info = parse_frame(raw_data)

            if self.image_width is None or self.image_height is None:
                self.image_width = info["width"]
                self.image_height = info["height"]

            if self.video_mode is None:
                try:
                    self.video_mode = video_modes[info["mode"]]
                except KeyError:
                    print(f"Unknown video format {info['mode']}")
                    exit()

            return data
