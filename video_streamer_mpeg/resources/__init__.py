import os


__this_file = os.path.realpath(__file__)
__this_path = os.path.dirname(__this_file)


def get_resource(resource):
    """Returns the path to go to a resource"""
    return os.path.join(__this_path, resource)
