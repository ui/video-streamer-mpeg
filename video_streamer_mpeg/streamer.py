import gevent.monkey

gevent.monkey.patch_all(thread=False)

import re  # noqa: E402
import numpy  # noqa: E402
import socket  # noqa: E402
import logging  # noqa: E402
import argparse  # noqa: E402
import os  # noqa: E402
import gevent  # noqa: E402
import ffmpeg  # noqa: E402
from geventwebsocket import (  # noqa: E402
    WebSocketError,
    WebSocketServer,
    WebSocketApplication,
    Resource,
)
from flask import Flask, send_file, abort  # noqa: E402

from .lima import LimaCamera  # noqa: E402
from .simulator import SimulatorCamera  # noqa: E402
from .jpeg import JPEGCamera  # noqa: E402
from . import __version__  # noqa: E402
from .resources import get_resource  # noqa: E402


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

for k in ["PIL.TiffImagePlugin", "urllib3.connectionpool"]:
    log = logging.getLogger(k)
    log.level = logging.INFO


cameras = {"lima": LimaCamera, "sim": SimulatorCamera, "jpeg": JPEGCamera}
flask_app = Flask(__name__)


@flask_app.route("/")
def index():
    return send_file(get_resource("client.html"))


@flask_app.route("/jsmpeg.min.js")
def jsmpeg():
    return send_file(get_resource("jsmpeg.min.js"))


@flask_app.route("/cursor.json")
def cursor():
    cursor = get_resource("cursor.json")
    if os.path.exists(cursor):
        return send_file(cursor)
    else:
        abort(404)


def start_ffmpeg(
    size,
    format,
    q,
    gamma=1,
    rotation=0,
    scale=None,
    crop=None,
    threads=None,
    save_frame=None,
):
    if rotation:
        # ffmpeg doesnt seems to know that rotations affect the pix format of bayer modes
        mat = re.match(r"^bayer_(\w\w\w\w)(\d+)$", format)
        if mat:
            matrix = numpy.array([[mat[1][0], mat[1][1]], [mat[1][2], mat[1][3]]])
            flat = "".join(numpy.rot90(matrix, -(rotation / 90)).flatten())
            format = f"bayer_{flat}{mat[2]}"
            print(f"  >> Bayer rotated format is {format}")

    job = ffmpeg.input(
        "pipe:0", format="rawvideo", s="{}x{}".format(size[0], size[1]), pix_fmt=format
    )

    if gamma != 1:
        job = job.filter("eq", gamma=gamma)

    if rotation:
        rotations = {90: 1, 270: 2}
        job = job.filter("transpose", rotations[rotation])

    if scale:
        job = job.filter("scale", scale, "-1")

    if crop:
        job = job.filter("crop", *crop.split(":"))

    extra_opts = {}
    if threads:
        extra_opts["threads"] = threads
        extra_opts["filter_threads"] = threads
        extra_opts["filter_complex_threads"] = threads

    if save_frame:
        job = job.output(save_frame, **{"vframes": 1, "q:v": q}).run_async(
            pipe_stdin=True, overwrite_output=True
        )
    else:
        job = job.output(
            "pipe:1",
            vcodec="mpeg1video",
            f="mpegts",
            an=None,
            **{"q:v": q},
            **extra_opts,
        ).run_async(pipe_stdin=True, pipe_stdout=True)

    # job = job.run_async(pipe_stdin=True, pipe_stdout=True, overwrite_output=True)

    return job


class Streamer(WebSocketApplication):
    def on_open(self):
        logger.info("Client connected")

    def on_close(self, reason):
        logger.info(f"Client disconnected: {reason}")


def emit(job, server):
    logger.info("Starting broadcaster")
    while True:
        try:
            out_bytes = job.stdout.read(128)
            clients = list(server.clients.values()).copy()
            for client in clients:
                try:
                    client.ws.send(out_bytes)

                except WebSocketError:
                    logger.exception("WebSocketError")
        except Exception as e:
            logger.warning(f"Could not read frame to emit {e}")
            gevent.sleep(1)


def read(job, camera):
    while True:
        try:
            data = camera.frame()
            job.stdin.write(data.tobytes())
            gevent.sleep(0.03)
        except KeyboardInterrupt:
            raise
        except Exception as e:
            logger.warning(f"Could not get camera frame: {e}")
            gevent.sleep(1)


def main(in_args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-b",
        "--bind",
        help="IP/Hostname of interface to bind to",
        default=socket.gethostname(),
    )
    parser.add_argument(
        "-p", "--port", help="The port to bind to", default=5000, type=int
    )
    parser.add_argument("-d", "--device", help="Device Address")
    parser.add_argument(
        "-t", "--type", help="Camera Type", default="lima", choices=list(cameras.keys())
    )

    parser.add_argument("-cert", "--cert", help="SSL Certificate", default=None)
    parser.add_argument("-key", "--key", help="SSL Key", default=None)

    parser.add_argument(
        "-ht", "--half-tone", help="Half-tone image to 8bit", action="store_true"
    )
    parser.add_argument(
        "--normalize", help="Normalize input data into 8bit", action="store_true"
    )
    parser.add_argument(
        "-q", "--quality", help="Video quality (bitrate)", default=4, type=int
    )
    parser.add_argument("-g", "--gamma", help="Adjust gamma", default=1, type=float)
    parser.add_argument("-bpp", "--bpp", help="Bits per pixel for rescaling", type=int)

    parser.add_argument(
        "-s",
        "--save",
        help="Save a raw frame to disk for debugging",
        action="store_true",
    )
    parser.add_argument("-j", "--save-jpeg", help="Save ffmpeg frame to jpeg for tests")
    parser.add_argument(
        "-r", "--rotation", help="Rotate the image (simulator only)", type=int
    )
    parser.add_argument(
        "--camera-args",
        metavar="KEY=VALUE",
        nargs="+",
        help="Set a number of key-value pairs for the camera",
    )
    parser.add_argument(
        "-sc",
        "--scale",
        help="Scale video to [scale] pixels wide, aspect ratio will be maintained",
        type=float,
    )
    parser.add_argument(
        "-c",
        "--crop",
        help="Crop video using the -crop filter (see https://ffmpeg.org/ffmpeg-filters.html#crop)",
        type=str,
    )
    parser.add_argument(
        "-th",
        "--threads",
        help="Limit to (n) threads",
        type=int,
    )

    args = parser.parse_args(in_args)

    camera_args = {}
    if args.camera_args:
        for pairs in args.camera_args:
            kv = pairs.split("=")
            if len(kv) > 1:
                camera_args[kv[0]] = kv[1]

    server_args = {}
    if args.cert and args.key:
        server_args["keyfile"] = args.key
        server_args["certfile"] = args.cert

    server = WebSocketServer(
        (args.bind, args.port),
        Resource([("^/video", Streamer), ("^/.*", flask_app)]),
        debug=False,
        **server_args,
    )

    camera = cameras[args.type](args.device, **camera_args)
    camera.set_options(args.half_tone, args.bpp, args.normalize)

    if args.rotation:
        if not args.type == "sim":
            print("Rotation can only be used with simulation device")
            exit(1)

        camera.set_rotation(args.rotation)

    if args.save:
        camera.raw_frame(save=True)
        print("Saved frame, exiting")
        exit(0)

    print(f"Starting video-streamer-mpeg version {__version__}")
    print(
        f"  Image format is {camera.pix_format()} converted to ffmpeg {camera.format()}, size: {camera.size()}, half-toning: {args.half_tone}, rotation: {camera.manual_rotation()}"
    )

    job = start_ffmpeg(
        camera.size(),
        camera.format(),
        args.quality,
        args.gamma,
        camera.manual_rotation(),
        args.scale,
        args.crop,
        args.threads,
        save_frame=args.save_jpeg,
    )

    gevent.spawn(emit, job, server)
    gevent.spawn(read, job, camera)

    if args.save_jpeg:
        job.wait()
        exit(0)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    print("Stopped")


if __name__ == "__main__":
    main()
