# -*- coding: utf-8 -*-

# Allows running the server with "python -m video_streamer_mpeg"
from video_streamer_mpeg.streamer import main

main()
